package street.executor;

import street.model.Vehicle;

public interface Executor {
    String getActionId();
    Vehicle execute(Vehicle[] positions, int index);
}
