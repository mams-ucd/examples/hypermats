package street.model;

import hypermats.core.model.Agent;
import hypermats.core.model.AgentDescription;
import street.simulation.StreetSimulation;

public class Vehicle extends Agent {
    private StreetSimulation simulation;
    private int location;
    private int speed = 0;

    public Vehicle(StreetSimulation simulation, AgentDescription agent) {
        super(agent);
        this.simulation = simulation;
    }

    public VehicleState getState(String host) {
        VehicleState state = new VehicleState();
        state.type = "street";
        state.url = simulation.getUrl(host);
        state.at = location;
        state.length = simulation.getStreet().length;
        state.speed = speed;
        return state;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getSpeed() {
        return speed;
    }

    public StreetSimulation getSimulation() {
        return simulation;
    }

    public void remove() {
        simulation.removeVehicle(this);
    }
}
