package street.model;

import java.util.LinkedList;

import hypermats.core.model.Link;

public class StreetDescription {
    public StreetDescription(StreetConfiguration configuration) {
        this.id = configuration.id;
        this.length = configuration.length;
    }

    public String id;
    public int length;
    public LinkedList<Link> in = new LinkedList<>();
    public LinkedList<Link> out = new LinkedList<>();
    public LinkedList<String> vehicles = new LinkedList<>();
}
