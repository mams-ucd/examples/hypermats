/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;

import mams.*;
import com.fasterxml.jackson.databind.JsonNode;

public class Main extends ASTRAClass {
	public Main() {
		setParents(new Class[] {mams.javatypes.Passive.class});
		addRule(new Rule(
			"Main", new int[] {10,9,10,28},
			new GoalEvent('+',
				new Goal(
					new Predicate("main", new Term[] {
						new Variable(Type.LIST, "args",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"Main", new int[] {10,27,15,5},
				new Statement[] {
					new ScopedSubgoal(
						"Main", new int[] {11,8,15,5},
						"MAMSAgent",
						new Goal(
							new Predicate("setup", new Term[] {})
						)
					),
					new ScopedSubgoal(
						"Main", new int[] {12,8,15,5},
						"MAMSAgent",
						new Goal(
							new Predicate("created", new Term[] {
								Primitive.newPrimitive("base")
							})
						)
					),
					new ScopedSubgoal(
						"Main", new int[] {14,8,15,5},
						"mams.javatypes.Passive",
						new Goal(
							new Predicate("listResource", new Term[] {
								Primitive.newPrimitive("agents"),
								Primitive.newPrimitive("hypermats.model.Agent")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"Main", new int[] {17,9,18,85},
			new GoalEvent('+',
				new Goal(
					new Predicate("handleNewListItemResource", new Term[] {
						new Variable(Type.STRING, "artifact_name",false),
						Primitive.newPrimitive("hypermats.model.Agent")
					})
				)
			),
			new Predicate("artifact", new Term[] {
				new Variable(Type.STRING, "artifact_name"),
				new Variable(Type.STRING, "qualified_name",false),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"Main", new int[] {18,84,26,5},
				new Statement[] {
					new ModuleCall("cartago",
						"Main", new int[] {19,8,19,55},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("getObject", new Term[] {
								new Variable(new ObjectType(Object.class), "object",false)
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("Main","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new Declaration(
						new Variable(Type.STRING, "name"),
						"Main", new int[] {21,8,26,5},
						new ModuleTerm("oa", Type.STRING,
							new Predicate("getString", new Term[] {
								new Variable(new ObjectType(Object.class), "object"),
								Primitive.newPrimitive("name")
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((astra.lang.ObjectAccess) intention.getModule("Main","oa")).getString(
										(java.lang.Object) intention.evaluate(predicate.getTerm(0)),
										(java.lang.String) intention.evaluate(predicate.getTerm(1))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((astra.lang.ObjectAccess) visitor.agent().getModule("Main","oa")).getString(
										(java.lang.Object) visitor.evaluate(predicate.getTerm(0)),
										(java.lang.String) visitor.evaluate(predicate.getTerm(1))
									);
								}
							}
						)
					),
					new Declaration(
						new Variable(new ObjectType(JsonNode.class), "init"),
						"Main", new int[] {22,8,26,5},
						new ModuleTerm("accessor", new ObjectType(com.fasterxml.jackson.databind.JsonNode.class),
							new Predicate("getJsonNode", new Term[] {
								new Variable(new ObjectType(Object.class), "object"),
								Primitive.newPrimitive("init")
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((hypermats.JsonNodeAccessor) intention.getModule("Main","accessor")).getJsonNode(
										(java.lang.Object) intention.evaluate(predicate.getTerm(0)),
										(java.lang.String) intention.evaluate(predicate.getTerm(1))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((hypermats.JsonNodeAccessor) visitor.agent().getModule("Main","accessor")).getJsonNode(
										(java.lang.Object) visitor.evaluate(predicate.getTerm(0)),
										(java.lang.String) visitor.evaluate(predicate.getTerm(1))
									);
								}
							}
						)
					),
					new ModuleCall("system",
						"Main", new int[] {24,8,24,52},
						new Predicate("createAgent", new Term[] {
							new Variable(Type.STRING, "name"),
							Primitive.newPrimitive("hypermats.Driver")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.System) intention.getModule("Main","system")).createAgent(
									(java.lang.String) intention.evaluate(predicate.getTerm(0)),
									(java.lang.String) intention.evaluate(predicate.getTerm(1))
								);
							}
						}
					),
					new ModuleCall("system",
						"Main", new int[] {25,8,25,46},
						new Predicate("setMainGoal", new Term[] {
							new Variable(Type.STRING, "name"),
							new ListTerm(new Term[] {
								new Funct("init", new Term[] {
									new Variable(new ObjectType(JsonNode.class), "init")
								})
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.System) intention.getModule("Main","system")).setMainGoal(
									(java.lang.String) intention.evaluate(predicate.getTerm(0)),
									(astra.term.ListTerm) intention.evaluate(predicate.getTerm(1))
								);
							}
						}
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("console",astra.lang.Console.class,agent);
		fragment.addModule("system",astra.lang.System.class,agent);
		fragment.addModule("oa",astra.lang.ObjectAccess.class,agent);
		fragment.addModule("accessor",hypermats.JsonNodeAccessor.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new TestSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new Main().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
