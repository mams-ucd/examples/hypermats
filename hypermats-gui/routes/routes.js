const express = require('express')

const router = express.Router()
const appController = require('../controllers/appController.js')

// Application Routes
router.get('/', appController.home)
// router.get('/streets', appController.streets)
// router.get('/junctions', appController.junctions)


module.exports = router
