package street.simulation;

import java.util.LinkedList;

import street.executor.Executor;
import street.executor.ExecutorFactory;
import street.model.Vehicle;

public class Lane {
    /**
     * The ordering of the vehicles on the road
     */
    private LinkedList<Vehicle> vehicles;

    /**
     * Length of road in metres
     */
    private double length;

    public Lane(double length) {
        vehicles = new LinkedList<Vehicle>();
        this.length = length;
    }

    /**
     * If there is no vehicle at the start of the road, add the vehicle...
     * @param vehicle
     * @return
     */
    public boolean join(Vehicle vehicle) {
        if (!vehicles.isEmpty() && vehicles.getFirst().atRoadStart()) return false;
        vehicles.addFirst(vehicle);
        return true;
    }

    /**
     * Assumption here: the maximum number of vehicles that can leave the street per
     * iteration is 1...
     * @return
     */
    public LinkedList<Vehicle> step(int duration) {
        LinkedList<Vehicle> exitingVehicles = new LinkedList<>();

        int i=vehicles.size()-1;
        System.out.println("i="+i);

        // for all positons, check if there is a vehicle,
        // and move it forwards the number of cells based on its speed.
        // auto slow down happens if it cannot move the full number of steps.
        Vehicle lastVehicle = null;
        while (i >= 0) {
            Vehicle vehicle = applyAgentAction(vehicles.get(i));
            System.out.println("Vehicle: " + vehicle.name);
            if (vehicle.speed > 0) {
                double mps = vehicle.speedInMps();
                double distanceToTravel = Math.round(mps * duration * 100) / 100.0;
                System.out.println("mps: " + mps);
                System.out.println("dtt: " + distanceToTravel);

                double distanceTravelled = 0;
                double totalDistanceTravelled = vehicle.totalDistanceTravelled + distanceToTravel;
                if ((lastVehicle == null) && (totalDistanceTravelled > length)) {
                    // we went off the end of the road...
                    totalDistanceTravelled = length;
                    distanceTravelled = length-vehicle.totalDistanceTravelled;
                    vehicle.totalDistanceTravelled = totalDistanceTravelled;
                    vehicle.distanceTravelled = distanceTravelled;
                    vehicle.distanceToTravel = Math.round((distanceToTravel-distanceTravelled)*100)/100.0;
                    exitingVehicles.add(vehicles.remove(i));
                    // exitingVehicles.put(vehicle.name, transferAgent(vehicle));
                } else {
                    if (lastVehicle == null) {
                        distanceTravelled = distanceToTravel;
                    } else {
                        double lastVehicleDistance = lastVehicle.totalDistanceTravelled;
                        if (totalDistanceTravelled > lastVehicleDistance) {
                            // we caught up with another car
                            // set the speed to be the same as the speed of that car...
                            totalDistanceTravelled = lastVehicleDistance-vehicle.getLength();
                            distanceTravelled = lastVehicleDistance-vehicle.totalDistanceTravelled;
                            vehicle.speed = lastVehicle.speed;
                        } else {
                            distanceTravelled = distanceToTravel;
                        }
                    }
                    vehicle.totalDistanceTravelled = totalDistanceTravelled;
                    vehicle.distanceTravelled = distanceTravelled;
                    lastVehicle = vehicle;
                }
            }
            i--;
        }

        return exitingVehicles;
    }

    private Vehicle applyAgentAction(Vehicle vehicle) {
        Executor executor = ExecutorFactory.get(vehicle.agent.getAction().id);
        if (executor == null) throw new RuntimeException("Unknown action: "  + vehicle.agent.getAction().id);
        return executor.execute(vehicle);
    }

    public String toString() {
        return "["+length+"] " + vehicles;
    }
}
