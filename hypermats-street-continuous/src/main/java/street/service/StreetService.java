package street.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import street.model.StreetConfiguration;
import street.simulation.StreetSimulation;

@Component
public class StreetService {
    Map<String, StreetSimulation> simulations = new HashMap<>();

    public StreetSimulation addStreet(StreetConfiguration street, String host) {
        StreetSimulation simulation = new StreetSimulation(street, host);
        simulations.put(street.id, simulation);
        return simulation;
    }

    public boolean hasStreet(String id) {
        return simulations.containsKey(id);
    }

    public StreetSimulation getSimulation(String id) {
        return simulations.get(id);
    }

    public Collection<StreetSimulation> getSimulations() {
        return simulations.values();
    }
}
