package street.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;

import hypermats.core.model.Action;
import hypermats.core.model.VehicleType;
import street.model.Vehicle;
import street.model.VehicleDescription;

@Component
public class VehicleService {
    Map<String, Vehicle> vehicles = new HashMap<>();

    public Vehicle add(VehicleDescription description, VehicleType type, String host) {
        Vehicle vehicle = new Vehicle(description, type, host);
        System.out.println("Storing Vehicle: "  + vehicle.name);
        vehicles.put(vehicle.name, vehicle);
        return vehicle;
    }

    public Vehicle remove(String id) {
        return vehicles.remove(id);
    }
    
    public Collection<Vehicle> getVehicles() {
        return vehicles.values();
    }

    public Vehicle getVehicle(String id) {
        return vehicles.get(id);
    }

    /**
     * Update state of vehicle when transferring from other service...
     * @param vehicle
     * @param node
     */
    public void initialize(Vehicle vehicle, JsonNode node) {
        if (node.has("state")) {
            JsonNode state = node.get("state");
            vehicle.distanceTravelled = state.get("distanceTravelled").asDouble();
            vehicle.distanceToTravel = state.get("distanceToTravel").asDouble();
            vehicle.speed = state.get("speed").asInt();
        }
        
        if (node.has("action")) {
            JsonNode actionNode = node.get("action");
            Action action = new Action(actionNode.get("id").asText());
            vehicle.agent.setAction(action);
        }
    }
}
