package street.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;

import hypermats.core.model.VehicleType;

@Component
public class VehicleTypeService {
    Map<String, VehicleType> types = new HashMap<>();

    public VehicleType get(String type) {
        if (types.containsKey(type)) return types.get(type);

        RestTemplate template = new RestTemplate();
        VehicleType vehicleType = template.getForEntity(type, VehicleType.class).getBody();
        types.put(type, vehicleType);
        return vehicleType;
    }

    public VehicleType fromJson(JsonNode typeNode) {
        String type = typeNode.get("@id").asText();

        // If we have it, use it
        if (types.containsKey(type)) return types.get(type);

        // See if the json is fully formed
        if (typeNode.has("name") && typeNode.has("length")) {
            VehicleType vehicleType = new VehicleType(type, typeNode.get("type").asText(), typeNode.get("length").asDouble());
            types.put(type, vehicleType);
            return vehicleType;
        }

        // GET it from the types service and cache it...
        RestTemplate template = new RestTemplate();
        JsonNode node = template.getForEntity(type, JsonNode.class).getBody();
        if (node == null) return null;
        
        VehicleType vehicleType = new VehicleType(node.get("@id").asText(), node.get("type").asText(), node.get("length").asDouble());
        types.put(type, vehicleType);
        return vehicleType;
    }
}
