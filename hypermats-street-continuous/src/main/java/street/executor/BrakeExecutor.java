package street.executor;

import street.model.Vehicle;

public class BrakeExecutor implements Executor {
    @Override
    public String getActionId() {
        return "brake";
    }

    @Override
    public Vehicle execute(Vehicle vehicle) {
        vehicle.speed = vehicle.speed-10;
        return vehicle;
    }
    
}
