package street.executor;

import street.model.Vehicle;

public class SkipExecutor implements Executor {
    @Override
    public String getActionId() {
        return "skip";
    }

    @Override
    public Vehicle execute(Vehicle vehicle) {
        return vehicle;
    }
    
}
