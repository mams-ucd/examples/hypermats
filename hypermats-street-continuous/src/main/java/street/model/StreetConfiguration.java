package street.model;

public class StreetConfiguration {
    public StreetConfiguration(String id, double length) {
        this.id = id;
        this.length = length;
    }

    public String id;
    public double length;
}
