import { LineString } from 'ol/geom';
import {lineString, lineIntersect} from '@turf/turf';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point.js';

export class ExportDataWrangler {

    constructor(source) {
        this.source = source;
        this.roads = [];
        this.junctions = [];
        this.parseRoadData();
        this.createJunctions();
    }

    parseRoadData() {
        for (let i = 0; i<this.source.length; i++) {
            let parsed_road = new Road(this.source[i]);
            this.roads.push(parsed_road);
        }
    }

    createJunctions() {
        for (let i = 0; i<this.roads.length; i++) {
            let road = this.roads[i];
            //Transform geometry of roads into different format for Turf (EPSG:4326)
            // (OpenLayers using EPSG:3857)
            let roadLineCoords = this.extractEPSG4326Coordinates(road.data.geometry.flatCoordinates);
            //Convert
            for (let j = 0; j<this.roads.length; j++) {
                if (i == j) {
                    //skip
                } else {
                    let road2 = this.roads[j];
                    let roadLineCoords2 = this.extractEPSG4326Coordinates(road2.data.geometry.flatCoordinates);
                
                    var intersects = lineIntersect(lineString(roadLineCoords), lineString(roadLineCoords2));
                
                    for (let k=0; k<intersects.features.length; k++) {
                        
                        //this.extractEPSG3857Coordinates(intersects.features[k].geometry.coordinates);
                        let coords = intersects.features[k].geometry.coordinates;
                        //Check isn't a duplicate
                        if (!this.existsAlready(road.id, road2.id, coords)) {
                            let j = new Junction(road.id, road2.id, coords);
                            this.junctions.push(j);
                        }
                    }
                }
            }
        }
    }

    existsAlready(id1, id2, coords) {
        for (let i = 0; i<this.junctions.length; i++) {
            let junct = this.junctions[i];
            if ((junct.road1 == id1 && junct.road2 == id2) || (junct.road2 == id1 && junct.road1 == id2)) {
                //Are the coordinates the same?
                if (junct.coordinates[0] == coords[0] && junct.coordinates[1] == coords[1]) {
                    return true;
                }
            }
        }
        return false;
    }

    getJunctionsAsFeatureCollection() {
        let features = [];
        for (let i = 0; i<this.junctions.length; i++) {
            features.push(new Feature({
                'geometry': new Point(this.junctions[i].coordinates),
                'size': 10.
            }));
        }
        return features;
    }

    extractEPSG4326Coordinates(coords) {
        let roadLine = new LineString(coords, "XY");
        roadLine.getSimplifiedGeometry().transform('EPSG:3857', 'EPSG:4326');
        return roadLine.getCoordinates();
    }

    writeToConsole() {
        console.log("****DUMPING****");
        console.log(JSON.stringify(this.roads));
        console.log(JSON.stringify(this.junctions));
    }

    writeExportableData() {
        let exportData = {};
        let streets = [];
        for (let i = 0; i<this.roads.length; i++) {
            streets.push(new StreetExport(this.roads[i]));
        }
        exportData.streets = streets;

        let junctions = [];
        let links = [];
        for (let i = 0; i<this.junctions.length; i++) {
            junctions.push(new JunctionExport(this.junctions[i]));
            let link1 = { 
                out: { type:"street", id:this.junctions[i].road1},
                in: { type:"junction", id:this.junctions[i].id}
            }
            let link2 = { 
                in: { type:"street", id:this.junctions[i].road1},
                out: { type:"junction", id:this.junctions[i].id}
            }
            let link3 = { 
                out: { type:"street", id:this.junctions[i].road2},
                in: { type:"junction", id:this.junctions[i].id}
            }
            let link4 = { 
                in: { type:"street", id:this.junctions[i].road2},
                out: { type:"junction", id:this.junctions[i].id}
            }
            links.push(link1,link2,link3,link4);
        }
        exportData.junctions = junctions;
        exportData.links = links;
        console.log("****DATA EXPORT READY****");
        return exportData;
    }
}

/**
 * Roads: object for client code
 */
class Road {
    
    constructor(source) {
        this.id = 0;
        this.coordinates = [];
        this.length = 0;
        this.data = source;
        
        this.parseRoad(source);
    }        

    parseRoad(source) {
        this.id = source.geometry.ol_uid;
        this.coordinates = source.geometry.flatCoordinates;
        let ls = new LineString(source.geometry.flatCoordinates, "XY");
        this.length = Math.round(ls.getLength());
    }
}
/**
 * Infer a junction where 2 roads meet: object for client code
 */
class Junction {
    constructor(road1Id, road2Id, coords) {
        this.id = road1Id+road2Id;
        this.road1 = road1Id;
        this.road2 = road2Id;
        this.coordinates = coords;
    }
}
/**
 * Classes for export to HyperMATS simulation
 *
 * StreetExport
 */
class StreetExport {
    constructor(road) {
        this.id = road.id;
        this.length = road.length;
    }
}
/**
 * JunctionExport
 */
class JunctionExport {
    constructor(junction) {
        this.id = junction.id;
        this.inLinks = 1;
        this.outLinks = 1;
    }
}