import {default as OLMap} from 'ol/Map.js';
import OSMXML from 'ol/format/OSMXML.js';
import View from 'ol/View.js';
import {Circle as CircleStyle, Stroke, Style} from 'ol/style.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
import {bbox as bboxStrategy} from 'ol/loadingstrategy.js';
import {transformExtent} from 'ol/proj.js';
import {OSM, Vector as VectorSource} from 'ol/source.js';
import Feature from 'ol/Feature';
import {Polygon} from 'ol/geom.js';
import Point from 'ol/geom/Point.js';
import {easeOut} from 'ol/easing.js';
import {getVectorContext} from 'ol/render.js';
import {unByKey} from 'ol/Observable.js';
import {ExportDataWrangler as DataWrangler} from './export_data_wrangler.mjs';
import {Convertor} from './convertor.mjs';
import {Agent} from './agent.mjs';
import Draw, {
  createBox
} from 'ol/interaction/Draw.js';
import {getArea} from 'ol/sphere.js';
import Overlay from 'ol/Overlay.js';

/**
 * Very Messy Client Code
 * 
 * Map from OSM, using Open Layers
 * Only works with one agent (for now)
 * 
 * EPSG:3857 - co-ordinate system https://epsg.io/3857
 * 
 * Multiple layers: 
 *  - the Map layer (OSM )
 *  - the Road layer (API call to https://overpass-api.de/api/interpreter)
 *  - the Junctions layer (inferred from roads)
 *  - the Agent layer (from the WebSocket server)
 *  - the "Draw simulation area" layer (user draws on map)
 * 
 * Each layer has a source, which can be a collection of Features, can be
 * GeoJSON, can be from created features of from an API call (as for roads)
 * 
 * TODO: when start simulation, shade in grey area that is not included
 * */
let map = null;
var roadFeatures = null;
const agentsMap = new Map();
let exportData = null;
let convertor = null;

/**
 * MAP OVERLAY: Roads
 * styles: how to draw the selected road types
 */
const roadStyles = {
  'highway': {
    '.*': new Style({
      stroke: new Stroke({
        color: 'rgba(120, 81, 169, 1.0)',
        width: 2,
      }),
    }),
  },
};
/**
 * MAP OVERLAY: Junctions
 * junctionStyles: how to draw the junction overlays
 */
const junctionStyles = {
  '10': new Style({
    image: new CircleStyle({
      radius: 5,
      //fill: new Fill({color: '#666666'}),
      stroke: new Stroke({color: '#bada55', width: 1}),
    }),
  }),
};

function setJunctionVectorSource(roadData) {
  var dw = new DataWrangler(roadData);
  //Clear any previous
  junctionVectorSource.clear();

  //Style all the junctions
  let junctions = dw.getJunctionsAsFeatureCollection();
  //Add new ones
  junctionVectorSource.addFeatures(junctions);
  exportData = dw.writeExportableData();
  convertor = new Convertor(dw.roads, dw.junctions);
}
//Road data comes from https://overpass-api.de/api/interpreter
function roadsToInclude() {
  let roadTypes = [];
  for (let i=1; i<8; i++) {
    let checkBox = document.getElementById("roadType" + i);
    if (checkBox.checked == true) {
      roadTypes.push(checkBox.value);
    }
  }
  return roadTypes;
}
let simulationBounds;
function roadLoader(extent, resolution, projection, success, failure) {
  let roadData = [];
  roadVectorSource.clear();
  if (simulationBounds) {
    extent = simulationBounds.extent_;
  }
  const epsg4326Extent = transformExtent(extent, projection, 'EPSG:4326');
  const client = new XMLHttpRequest();
  client.open('POST', 'https://overpass-api.de/api/interpreter');
  client.addEventListener('load', function () {
    roadFeatures = new OSMXML().readFeatures(client.responseText, {
      featureProjection: map.getView().getProjection(),
    });
    
    let roadFeaturesToInclude = [];
    let roadTypes = roadsToInclude();
    //We have the road data, now filter and infer junctions 
    // (this is in data_wrangler.mjs)
    for (let i=0; i<roadFeatures.length; i++) {
      var f = roadFeatures[i].values_;
      if (f.highway) {
        
        for (let j=0; j<roadTypes.length; j++) {
          if (f.highway == roadTypes[j]) {
            roadData.push(f); //TODO sort out having multiple arrays here
            roadFeaturesToInclude.push(roadFeatures[i]);
          }
        }
      }
    }
    setJunctionVectorSource(roadData);

    roadVectorSource.addFeatures(roadFeaturesToInclude);
    success(roadFeaturesToInclude);
  });
  client.addEventListener('error', failure);
  const query =
    '(node(' +
    epsg4326Extent[1] +
    ',' +
    Math.max(epsg4326Extent[0], -180) +
    ',' +
    epsg4326Extent[3] +
    ',' +
    Math.min(epsg4326Extent[2], 180) +
    ');rel(bn)->.foo;way(bn);node(w)->.foo;rel(bw););out meta;';
  client.send(query);
}
const roadVectorSource = new VectorSource({
  format: new OSMXML(),
  loader: roadLoader,
  strategy: bboxStrategy,
});
/* 
 * roadVectorLayer: the vector to overlay on the map for the road/ parking etc layer
 */
const roadVectorLayer = new VectorLayer({
  source: roadVectorSource,
  style: function (feature) {
    for (const key in roadStyles) {
      const value = feature.get(key);
      if (value !== undefined) {
        for (const regexp in roadStyles[key]) {
          if (new RegExp(regexp).test(value)) {
            return roadStyles[key][regexp];
          }
        }
      }
    }
    return null;
  },
});


/* 
 * junctionvectorSource: the features start as an empty array and get added to,
 * once the roads have been parsed. This is as the roads are added after an API call,
 * then the junctions are inferred (in data_wrangler.mjs)
 */
const junctionVectorSource = new VectorSource({
  features: [], //These will be added to in another method
  wrapX: false
})

const junctionVectorLayer = new VectorLayer({
  source: junctionVectorSource,
  style: function (feature) {
    return junctionStyles[feature.get('size')];
  },
});

/** 
 * AGENT LAYERS
 * agentVectorSource: the source for the agent, which gets updated in addFlashingAgentFeature
 */
const agentVectorSource = new VectorSource({
  wrapX: false,
});

/* 
 * agentVectorLayer: the vector to overlay on the map for the agents
 */
const agentVectorLayer = new VectorLayer({
  source: agentVectorSource
});

/**
 * DRAW SIMULATION OVERLAY
 */
const drawSimSource = new VectorSource();

const drawSimVector = new VectorLayer({
  source: drawSimSource,
  style: {
    'fill-color': 'rgba(255, 255, 255, 0.2)',
    'stroke-color': '#ffcc33',
    'stroke-width': 2,
    'circle-radius': 7,
    'circle-fill-color': '#ffcc33',
  },
});

/*
 * MAP OVERLAY: Map actual
 */
const osm_raster = new TileLayer({
  source: new OSM(),
});

function convertCoordinates(lon, lat) {
  var x = (lon * 20037508.34) / 180;
  var y = Math.log(Math.tan(((90 + lat) * Math.PI) / 360)) / (Math.PI / 180);
  y = (y * 20037508.34) / 180;
  console.log("lon: ",lon," to ",x);
  console.log("lat: ",lat," to ",y);
  return [x, y];
}

map = new OLMap({
  layers: [osm_raster,roadVectorLayer,junctionVectorLayer,drawSimVector,agentVectorLayer],
  target: document.getElementById('map'),
  view: new View({
    //center: [-692561.7775832299,7040089.459624766], //UCD Belfield Post Office     
    center: convertCoordinates(-73.985428, 40.748817), 
    maxZoom: 19,
    zoom: 18,
  }),
});

/*
 * AGENT FLASHING
 * addFlashingAgentFeature: clear existing features from the source,
 * and add the updated agent location
 */
function flashAgent(value, key, map) {
  //key is agent ID
  //value is Agent object
  console.log("Flashing: ", {value});
  
  var agentLat = parseFloat(value.lat);
  var agentLon = parseFloat(value.lon);
  
  const geom = new Point([agentLon,agentLat]);
  const feature = new Feature(geom);
  agentVectorSource.addFeature(feature);
}

function addFlashingAgentFeature() {
  agentVectorSource.clear();
  agentsMap.forEach(flashAgent);
}

const duration = 3000;
function flash(feature) {
  const start = Date.now();
  const flashGeom = feature.getGeometry().clone();
  const listenerKey = osm_raster.on('postrender', animate);

  function animate(event) {
    const frameState = event.frameState;
    const elapsed = frameState.time - start;
    if (elapsed >= duration) {
      unByKey(listenerKey);
      return;
    }
    const vectorContext = getVectorContext(event);
    const elapsedRatio = elapsed / duration;
    // radius will be 5 at start and 30 at end.
    const radius = easeOut(elapsedRatio) * 25 + 5;
    const opacity = easeOut(1 - elapsedRatio);
    // the agent beacon
    const style = new Style({
      image: new CircleStyle({
        radius: radius,
        stroke: new Stroke({
          color: 'rgba(255, 0, 0, ' + opacity + ')',
          width: 0.25 + opacity,
        }),
      }),
    });
    
    vectorContext.setStyle(style);
    vectorContext.drawGeometry(flashGeom);
    // tell OpenLayers to continue postrender animation
    map.render();
  }
}

/*
 * FLASHING AGENT BEACON - listen to changes to agentVectorSource
 * and when addFeature is called, make it flash
 */
agentVectorSource.on('addfeature', function (e) {
  flash(e.feature);
});

/*
 * Draw simulation area code: from https://openlayers.org/en/latest/examples/measure.html
 * shape from:https://openlayers.org/en/latest/examples/draw-shapes.html
 */
/**
 * Currently drawn feature.
 * @type {import("/ol/Feature.js").default}
 */
let sketch;

/**
 * The help tooltip element.
 * @type {HTMLElement}
 */
let helpTooltipElement;

/**
 * Overlay to show the help messages.
 * @type {Overlay}
 */
let helpTooltip;

/**
 * The measure tooltip element.
 * @type {HTMLElement}
 */
let measureTooltipElement;

/**
 * Overlay to show the measurement.
 * @type {Overlay}
 */
let measureTooltip;

/**
 * Message to show when the user is drawing a polygon.
 * @type {string}
 */
const continuePolygonMsg = 'Click to continue drawing the rectangle';

/**
 * Handle pointer move.
 * @param {import("ol/MapBrowserEvent").default} evt The event.
 */
const pointerMoveHandler = function (evt) {
  if (evt.dragging) {
    return;
  }

  let helpMsg = 'Click to start drawing';

  if (sketch) {
    const geom = sketch.getGeometry();
    if (geom instanceof Polygon) {
      helpMsg = continuePolygonMsg;
    }
  }

  helpTooltipElement.innerHTML = helpMsg;
  helpTooltip.setPosition(evt.coordinate);

  helpTooltipElement.classList.remove('hidden');
};


map.on('pointermove', pointerMoveHandler);

map.getViewport().addEventListener('mouseout', function () {
  helpTooltipElement.classList.add('hidden');
});

const typeSelect = document.getElementById('type');

let draw; // global so we can remove it later

/**
 * Format area output.
 * @param {Polygon} box The box.
 * @return {string} Formatted area.
 */
const formatArea = function (box) {
  const area = getArea(box);
  let output;
  if (area > 10000) {
    output = Math.round((area / 1000000) * 100) / 100 + ' ' + 'km<sup>2</sup>';
  } else {
    output = Math.round(area * 100) / 100 + ' ' + 'm<sup>2</sup>';
  }
  return output;
};

function addInteraction() {
  draw = new Draw({
    source: drawSimSource,
    type: 'Circle',
    geometryFunction: createBox()
  });
  map.addInteraction(draw);

  createMeasureTooltip();
  createHelpTooltip();

  let listener;
  draw.on('drawstart', function (evt) {
    //Clear existing
    drawSimSource.clear();
    //Clear previous measurement tooltips

    // set sketch
    sketch = evt.feature;

    /** @type {import("ol/coordinate.js").Coordinate|undefined} */
    let tooltipCoord = evt.coordinate;

    listener = sketch.getGeometry().on('change', function (evt) {
      const geom = evt.target;
      let output;
      if (geom instanceof Polygon) {
            //Remember the limits of the area
        simulationBounds = geom;
        output = formatArea(geom);
        tooltipCoord = geom.getInteriorPoint().getCoordinates();
      } else {
        console.log("Error with geometry");
      }
      measureTooltipElement.innerHTML = output;
      measureTooltip.setPosition(tooltipCoord);
    });
  });

  draw.on('drawend', function () {
    measureTooltipElement.className = 'ol-tooltip ol-tooltip-static';
    measureTooltip.setOffset([0, -7]);
    // unset sketch
    sketch = null;
    // unset tooltip so that a new one can be created
    //measureTooltipElement = null; //Uncomment to leave in. Interesting, but 
    //litters screen with tooltips unless I figure out how to clear previous properly.
    createMeasureTooltip();
    unByKey(listener);
  });
}

/**
 * Creates a new help tooltip
 */
function createHelpTooltip() {
  if (helpTooltipElement) {
    helpTooltipElement.parentNode.removeChild(helpTooltipElement);
  }
  helpTooltipElement = document.createElement('div');
  helpTooltipElement.className = 'ol-tooltip hidden';
  helpTooltip = new Overlay({
    element: helpTooltipElement,
    offset: [15, 0],
    positioning: 'center-left',
  });
  map.addOverlay(helpTooltip);
}

/**
 * Creates a new measure tooltip
 */
function createMeasureTooltip() {
  if (measureTooltipElement) {
    measureTooltipElement.parentNode.removeChild(measureTooltipElement);
  }
  measureTooltipElement = document.createElement('div');
  measureTooltipElement.className = 'ol-tooltip ol-tooltip-measure';
  measureTooltip = new Overlay({
    element: measureTooltipElement,
    offset: [0, -15],
    positioning: 'bottom-center',
    stopEvent: false,
    insertFirst: false,
  });
  map.addOverlay(measureTooltip);
}

addInteraction();

/*
 * START SIMULATION
 */
let socket;
//map: agentName to interval ID : this is for the flashing agent stuff
// scope needs to be outside of the websocket stuff
const agentIntervalMap = new Map();
const delay = ms => new Promise(res => setTimeout(res, ms));
const waitForData = async () => {
  console.log("10 seconds to collect export data");
  await delay(15000);
  console.log("Export data ready... or too slow");
}
  
document.getElementById('start-simulation').onclick = function () {
  console.log("START");
  //roadVectorSource.refresh(); //FIXME: this will need fixing
  //need a pause here
  waitForData();
  socket = new WebSocket("ws://localhost:9898")
  
  //SEND DATA
socket.onopen = (event) => {
    var client = {
      type: "client",
    }
    socket.send(JSON.stringify(client));

    var data = {
      type: "data",
      body: exportData
    };
    console.log(JSON.stringify(data));
    socket.send(JSON.stringify(data));
    
  };

  /*
  * SERVER INTERACTION: AGENT BEACON
  */
 const delay = ms => new Promise(res => setTimeout(res, ms));
  socket.onmessage = (event) => {
    
    let agentData = JSON.parse(event.data);
    var agentName = agentData.agent;
    var placeId = agentData.id;
    var length = agentData.distance;
    var coords = convertor.getCoordinates(placeId, length);
    if (coords) {
      var agentLon = coords[0];
      var agentLat = coords[1];
      agentsMap.set(agentName, new Agent(agentName, agentLat, agentLon));
      console.log("Updating agents with " + agentName + " at lat: " + agentLat + " lon: " + agentLon);
      
      //Clear previous flashing agent before starting a new one
      var intervalIDOld = agentIntervalMap.get(agentName);
      if (intervalIDOld > 0) {
        window.clearInterval(intervalIDOld);
      }  
      
      var intervalId = window.setInterval(addFlashingAgentFeature, 2000);
      agentIntervalMap.set(agentName, intervalId);
    } else {
      console.log("Error getting co-ordinates for agent " + agentName);
    }
  }
};

document.getElementById('reset-simulation').onclick = function () {
  console.log("STOPPING");
  socket.send("STOP");
  socket.close();
  //Clear box
  drawSimSource.clear();
  simulationBounds = null;
  roadVectorSource.refresh();
  agentIntervalMap.forEach((value) => { window.clearInterval(value);} );
};

