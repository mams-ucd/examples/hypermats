package hypermats.core.model;

public class Action {
    public String id="";
    public Object[] parameters;

    public Action(String id, Object[] parameters) {
        this.id = id;
        this.parameters = parameters;
    }

    public Action(String id) {
        this.id = id;
        parameters = new Object[0];
    }

    public Action() {}

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(id).append("(");
        if (parameters != null) {
            boolean first = true;
            for (Object parameter : parameters) {
                if (first) first=false; else builder.append(",");
                builder.append(parameter);
            }
        }
        builder.append(")");
        return builder.toString();
    }
}
